package exemple.test.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionController {
	@ExceptionHandler(value = UserNotfoundException.class)
	public ResponseEntity<Object> exception(UserNotfoundException exception) {
		return new ResponseEntity<>("User Not Found", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = UserAlreadyExistException.class)
	public ResponseEntity<Object> exception(UserAlreadyExistException exception) {
		return new ResponseEntity<>("User Already Exist", HttpStatus.BAD_REQUEST);
	}
	
}

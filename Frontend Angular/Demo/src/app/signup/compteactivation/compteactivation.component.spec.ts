import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteactivationComponent } from './compteactivation.component';

describe('CompteactivationComponent', () => {
  let component: CompteactivationComponent;
  let fixture: ComponentFixture<CompteactivationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteactivationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteactivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

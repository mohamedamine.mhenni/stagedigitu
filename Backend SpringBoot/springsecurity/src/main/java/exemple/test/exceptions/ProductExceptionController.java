package exemple.test.exceptions;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ProductExceptionController {
	@ExceptionHandler(value = ProductNotfoundException.class)
	public ResponseEntity<Object> exception(ProductNotfoundException exception) {
		return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = ProductAlreadyExistException.class)
	public ResponseEntity<Object> exception(ProductAlreadyExistException exception) {
		return new ResponseEntity<>("Product Already Exist", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value=NoDataFoundException.class)
	public ResponseEntity<Object> exception(NoDataFoundException exception){

		return new ResponseEntity<>("No Data Found", HttpStatus.NOT_FOUND);
    }
}

package exemple.test.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import exemple.test.exceptions.UserNotfoundException;
import exemple.test.models.*;
import exemple.test.repository.UserRepository;
import exemple.test.service.MyUserDetailsService;
import exemple.test.service.UserService;
import exemple.test.util.JwtUtil;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/api/signup")
public class RegistrationController {
	@Autowired
	UserService userservice;
	@Autowired
	UserRepository userrepository;
	@Autowired
	public JavaMailSender emailSender;

	@PostMapping("/registration")
	public ResponseEntity<Object> addUser(@RequestBody User user) {
		String generatedString = RandomStringUtils.randomAlphanumeric(5);
		System.out.println(generatedString);
		user.setId(getNewId());
		user.getRoles().add("CLIENT");
		user.setActivationCode(generatedString);
		userservice.addUser(user);

		System.out.println(sendSimpleEmail(generatedString, user.getEmail()));
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@PostMapping("/activate")
	public Boolean activateAccount(@RequestBody ActivateResponse response) {
		Optional<User> u = userrepository.findById(response.getId());
		if (!u.isPresent())
			throw new UserNotfoundException();
		if (u.get().getActivationCode().equals(response.getCode())) {
			u.get().setStatus(true);
			userrepository.save(u.get());
			return true;
		}
		return false;
	}

	@GetMapping("/resendMail/{idUser}")
	public Boolean resendMail(@PathVariable int idUser) {
		String generatedString = RandomStringUtils.randomAlphanumeric(5);
		Optional<User> u = userrepository.findById(idUser);
		if(!u.isPresent())
			throw new UserNotfoundException();
		
		u.get().setActivationCode(generatedString);
		userrepository.save(u.get());
		sendSimpleEmail(generatedString, u.get().getEmail());
		return true;
	}

	public int getNewId() {
		List<User> lc = this.userrepository.findAll().stream().sorted(Comparator.comparing(User::getId))
				.collect(Collectors.toList());
		System.out.println(lc);
		return lc.get(lc.size() - 1).getId() + 1;
	}

	public String sendSimpleEmail(String code, String email) {

		// Create a Simple MailMessage.
		SimpleMailMessage message = new SimpleMailMessage();

		message.setTo(email);

		message.setSubject("Activation Code");
		message.setText("Voici votre code d'activation : " + code);

		// Send Message!
		this.emailSender.send(message);

		return "Email Sent!";
	}

}

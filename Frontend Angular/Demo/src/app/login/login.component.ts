import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../Services/LoginService';

import {Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;
  token: any;
  private returnUrl: string;
  hide=true;

  constructor(private authservice: LoginService, public router: Router) {
    this.userForm = new FormGroup(
      {
        email: new FormControl('', [Validators.required, Validators.minLength(3)]),
        password: new FormControl('', [Validators.required]),
      });
    if (this.authservice.currentUserValue) {
      this.router.navigate(['/home']);
    }

  }

  get getEmail() {
    return this.userForm.get('email');
  }

  get getPassword() {
    return this.userForm.get('password');
  }

  ngOnInit() {

  }

  getUserform() {
    console.log(this.userForm.value);
  }

  authenticate() {
    let authReq = {'email': this.getEmail.value, 'password': this.getPassword.value};
    this.authservice.Authenticate(authReq).subscribe((data: {}) => {this.router.navigate(["/user"]);});

  }


}

export class Category {
  public static fromJson(json: Object): Category {
    return new Category(
      json['id'],
      json['title'],
      json['description'],
      json['parent'],
      json['idParent']

    );
  }

  constructor(public id: bigint,
              public title: string,
              public description: string,
              public parent: string,
              public idCategoryParent: bigint) {
  }
}

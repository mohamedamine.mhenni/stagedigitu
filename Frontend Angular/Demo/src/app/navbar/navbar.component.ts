import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {LoginService} from '../Services/LoginService';
import {UserService} from "../Services/UserService";
import {WebSocketService} from "../Services/WebSocketService";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public static notif;
  text: any;
  hidden = false;
  //public notifications :any;
  @Output() Textout: EventEmitter<any> = new EventEmitter();
  searchForm: FormGroup;
  role: string = "";
  notifications;
  username: string;
  private stompClient: any;

  constructor(private Auth: LoginService, private userService: UserService, private webSocketService: WebSocketService) {
    this.role = Auth.currentUserValue.role;

    this.username = Auth.currentUserValue.username;
    this.searchForm = new FormGroup({
      searchbox: new FormControl('')
    });


  }

  get getSearchText() {
    return this.searchForm.get('searchbox');
  }

  @Input() Textbox() {
    this.text = this.getSearchText.value;
    this.Textout.emit(this.text);
  };

  ngOnInit() {
    this.userService.getUserById(this.Auth.currentUserValue.id).subscribe(data => {
      this.username = data['username']
    });

   // this.getNotification();
  }

  Find() {
    this.Textbox = this.getSearchText.value;
    this.Textout.emit(this.Textbox);
    console.log(this.Textbox + "*************")
  }

  logout() {
    this.Auth.logout();
  }

  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
    this.webSocketService.notifications=0;
  }

  getNotification() {
    this.stompClient = this.webSocketService.connect();
    this.stompClient.connect({}, frame => {
      // Subscribe to notification topic
      this.stompClient.subscribe('/topic/notification', notifications => {
        console.log(notifications);
        // Update notifications attribute with the recent messsage sent from the server
        this.notifications= this.notifications+1;
      });

    });
  }

}

package exemple.test.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exemple.test.exceptions.ProductNotfoundException;
import exemple.test.exceptions.UserNotfoundException;
import exemple.test.models.*;
import exemple.test.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productrepository;
	@Autowired
	private CategoryService categoryservice;
	@Autowired
	private UserService userservice;

	public List<Product> getAllProducts(int idUser) {
		if(userservice.getUserById(idUser).getRoles().contains("ADMIN"))
			return productrepository.findAll();
		return productrepository.findProductByIdUser(idUser);

	}

	public Product getProductById(int idProduct) {

		Optional<Product> product = productrepository.findById(idProduct);
		if (!product.isPresent())
			throw new ProductNotfoundException();

		return product.get();

	}
	public List<Product> getProductByTitle(String  title) {

		List<Product> products = new ArrayList<>();
		products.addAll(productrepository.findProductByTitle(title));
		

		return products;

	}

	public Product getProductUserById(int idUser, int idProduct) {
		userservice.getUserById(idUser);
		Optional<Product> product = productrepository.findProductByIdUser(idUser).stream()
				.filter(p -> p.getId() == idProduct).findFirst();
		if (!product.isPresent())
			throw new ProductNotfoundException();

		return product.get();

	}

	public Boolean addProduct(Product product,int IdCat) {
		User user = userservice.getUserById(product.getIdUser());
		Category cat = categoryservice.getCategoryById(IdCat);
		
		product.getCategories().add(cat);
		product.setIdUser(user.getId());
		product = productrepository.save(product);
		user.getProducts().add(product);
		// userrepository.save(user);
		userservice.updateUser(user);
		return true;
	}

	public void updateProduct(Product product) {
		Optional< Product > prod = productrepository.findById(product.getId());
		if(!prod.isPresent())
			throw new ProductNotfoundException();
		prod.get().setDescription(product.getDescription());
		prod.get().setTitle(product.getTitle());
		productrepository.save(prod.get());
		
	}

	public void deleteProduct(int idProduct) {
		Optional<Product> prod = productrepository.findById(idProduct);
		if(!prod.isPresent()) {
			throw new ProductNotfoundException();
		}
		productrepository.delete(prod.get());
		
	}

}

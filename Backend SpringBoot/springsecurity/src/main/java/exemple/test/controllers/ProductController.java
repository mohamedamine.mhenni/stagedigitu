package exemple.test.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exemple.test.exceptions.UserNotfoundException;

import exemple.test.models.*;
import exemple.test.repository.ProductRepository;
import exemple.test.repository.UserRepository;
import exemple.test.service.ProductService;

@RestController
@RequestMapping("/api/products")
public class ProductController {

	@Autowired
	ProductRepository productrepository;
	@Autowired
	UserRepository userrepository;
	@Autowired
	private SimpMessagingTemplate template;

	// Initialize Notifications
	private Notifications notifications = new Notifications(0);
	@Autowired
	ProductService productservice;

	@GetMapping(value = "/allproduct/{idUser}")
	public ResponseEntity<Object> getAllProducts(@PathVariable int idUser) {
		Optional<User> u = userrepository.findById(idUser);
		if (!u.isPresent()) {
			throw new UserNotfoundException();
		}

		return new ResponseEntity<>(productservice.getAllProducts(idUser), HttpStatus.OK);

	}

	@PostMapping("ByCategories/{idUser}")
	public List<Product> getProductByCategories(@RequestBody List<Category> lc, @PathVariable int idUser) {
		List<Product> lp;
		System.out.println(lc);
		if (userrepository.findById(idUser).get().getRoles().contains("ADMIN"))
			lp = this.productrepository.findAll();
		else
			lp = productservice.getAllProducts(idUser);
		List<Product> lpf = new ArrayList<>();
		for (Product p : lp) {
			for (Category c : p.getCategories()) {
				for (Category c1 : lc) {
					if (c.getId() == c1.getId())
						lpf.add(p);
				}
			}
		}
		System.out.println(lpf);
		return lpf;

	}

	@GetMapping("ById/{idProduct}")
	public Product getProductById(@PathVariable int idProduct) {
		return productservice.getProductById(idProduct);
	}

	@GetMapping("ByTitle/{title}")
	public List<Product> getProductByTitle(@PathVariable String title) {
		return productservice.getProductByTitle(title);
	}

	public int getNewId() {
		List<Product> lc = this.productrepository.findAll().stream().sorted(Comparator.comparing(Product::getId))
				.collect(Collectors.toList());
		System.out.println(lc);
		return lc.get(lc.size() - 1).getId() + 1;
	}

	@GetMapping(value = "/{idUser}/{idProduct}")
	public ResponseEntity<Object> getProductById(@PathVariable int idUser, @PathVariable int idProduct) {

		Product p = productservice.getProductUserById(idUser, idProduct);

		return new ResponseEntity<>(p, HttpStatus.OK);

	}

	@PostMapping(value = "/addProduct/{idCategroy}")
	public ResponseEntity<Object> addProduct(@RequestBody Product product, @PathVariable int idCategroy) {
		product.setId(this.getNewId());
		User u = userrepository.findById(product.getIdUser()).get();
		notifications.increment();
		
		notifications.getMessage().add(u.getUsername()+"  Add product");
		template.convertAndSend("/topic/notification", notifications);
		return new ResponseEntity<>(productservice.addProduct(product, idCategroy), HttpStatus.OK);
	}

	@DeleteMapping(value = "deleteProduct/{idProduct}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Object> deleteProductById(@PathVariable int idProduct) {
		productservice.deleteProduct(idProduct);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping("updateProduct")
	public void updateProduct(@RequestBody Product product) {
		productservice.updateProduct(product);
	}

}

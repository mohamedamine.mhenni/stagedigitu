import {Injectable} from '@angular/core';

import * as SockJs from 'sockjs-client';
import * as Stomp from 'stompjs';

@Injectable()
export class WebSocketService {
  public notifications=0;
  public msg:any=[];
  private socket: any;
  public stompClient: any;
  constructor(){
    this.initializeWebSocketConnection();
    console.log(this.msg)
  }
  // Open connection with the back-end socket
  public connect() {
      if(this.socket==null)
       {
         this.socket = new SockJs("http://localhost:8080/socket");

         this.stompClient = Stomp.over(this.socket);}

    return this.stompClient;
  }
  initializeWebSocketConnection() {
    const serverUrl = 'http://localhost:8080/socket';
    const ws = new SockJs(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/topic/notification', (message) => {
        if (message.body) {
          //that.notifications= JSON.parse(message.body).count;
          that.notifications=that.notifications+1;
          that.msg=JSON.parse(message.body).message;
        }
      });
    });
  }
}

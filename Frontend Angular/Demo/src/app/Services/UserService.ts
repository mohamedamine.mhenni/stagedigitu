import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn:'root'
})
export class UserService {



  public  constructor(private http:HttpClient ){

  }
  getAllUsers(): Observable<any> {
    return this.http.get<any>('api/users/all')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  getAllUsersByUserName(text): Observable<any> {
    return this.http.get<any>('api/users//ByUserName/'+text)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  addUser(User): Observable<any> {
    return this.http.post<any>('api/users/add',User)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  deleteUser(id){
    return this.http.delete<any>('api/users/'+id)
  }
  getUserById(id): Observable<any> {
    return this.http.get<any>('api/users/ById/'+id)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  UpdateUser(user): Observable<any> {
    console.log(user);
    return this.http.put<any>('/api/users/updateUser', user)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

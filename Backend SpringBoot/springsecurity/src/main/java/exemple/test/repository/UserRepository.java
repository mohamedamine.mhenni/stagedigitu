package exemple.test.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import exemple.test.models.User;

public interface UserRepository extends MongoRepository<User,Integer>{
	public Optional<User> findUserByEmail(String email);
	public Optional<User> findByUsername(String username);
	@Query(value = "{'username': {$regex : ?0, $options: 'i'}}")
	public List<User> findByUserName(String username);
}

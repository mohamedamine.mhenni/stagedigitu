package exemple.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;


import exemple.test.util.JwtUtil;
import junit.framework.Assert;

@TestInstance(Lifecycle.PER_CLASS)
@SpringBootTest
class UserApplicationTests {
	@Autowired
	JwtUtil jwtUtil;
	@Autowired
	private WebApplicationContext context;
	 
	private MockMvc mvc;
	@Autowired
	private ObjectMapper mapper;

	public Collection<? extends GrantedAuthority> getAuthorities(List<String> roles) {
		return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

	@BeforeAll
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}
	@Test
	public void testgetUsers() throws Exception {
		
		final String baseUrl = "http://ferdaws-dev.digit-dev.com/auth?email=badrouch922@gmail.com&password=091406&profile=PARENT";
	    URI uri = new URI(baseUrl);
	   // HttpHeaders headers = new HttpHeaders();
	    RestTemplate restTemplate = new RestTemplate();
	//    HttpEntity<Null> request = new HttpEntity<>(null, headers);
	    ResponseEntity<Object> result = restTemplate.postForEntity(uri, null,Object.class);
	    System.out.println(result.getBody());
	    Assert.assertEquals(200, result.getStatusCodeValue());
		
		
		
//		MvcResult result = mvc
//				.perform(post("/auth?email=badrouch922@gmail.com&password=091406&profile=PARENT")
//						.with(req -> {
//                    req.setServerName("http://ferdaws-dev.digit-dev.com");
//                    return req;
//                }))
//				
//				.andDo(print())
//				.andExpect(status().isOk()).andReturn();
	}
	
		
//		List<String> roles = new ArrayList<String>();
//		roles.add("ADMIN");
//		
//		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty",getAuthorities(roles)));
//		System.out.println(token);
//		assertNotNull(token);
//		MvcResult result = mvc.perform(get("/api/users/all").header("Authorization","Bearer "+ token)).andExpect(status().isOk()).andReturn();
//		
//		assertEquals(200,result.getResponse().getStatus());
	}
	
//	@Test
//	public void testgetUserById() throws Exception {
//		List<String> roles = new ArrayList<String>();
//		roles.add("ADMIN");
//
//		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
//		System.out.println(token);
//		assertNotNull(token);
//		MvcResult result = mvc.perform(get("/api/users/ById/54456").header("Authorization", "Bearer " + token))
//				.andExpect(status().isOk()).andReturn();
//
//		assertEquals(200, result.getResponse().getStatus());
//	}
//	@Test
//	public void testAddUser() throws Exception {
//		List<String> roles = new ArrayList<String>();
//		roles.add("ADMIN");
//		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
//		exemple.test.models.User user = new exemple.test.models.User(43,"mohamed amine","mhenni","test@test.com","azerty123");
//		String jsonRequest = mapper.writeValueAsString(user);
//		System.out.println(token);
//		assertNotNull(token);
//		MvcResult result = mvc.perform(post("/api/users/add")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonRequest)
//				.header("Authorization", "Bearer " + token))
//				.andExpect(status().isOk()).andReturn();
//
//		assertEquals(200, result.getResponse().getStatus());
//	}
//	@Test
//	public void testDeleteUser() throws Exception {
//		List<String> roles = new ArrayList<String>();
//		roles.add("ADMIN");
//		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
//		System.out.println(token);
//		assertNotNull(token);
//		MvcResult result = mvc.perform(delete("/api/users/3")
//				.header("Authorization", "Bearer " + token))
//				.andExpect(status().isOk()).andReturn();
//
//		assertEquals(200, result.getResponse().getStatus());
//	}
//	@Test
//	public void testUpdateUser() throws Exception {
//		List<String> roles = new ArrayList<String>();
//		roles.add("ADMIN");
//		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
//		exemple.test.models.User user = new exemple.test.models.User(54416,"mohamed amine","mhenni","test@test.com","azerty123");
//		String jsonRequest = mapper.writeValueAsString(user);
//		assertNotNull(token);
//		MvcResult result = mvc.perform(put("/api/users/updateUser")
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(jsonRequest)
//				.header("Authorization", "Bearer " + token))
//				.andExpect(status().isOk()).andReturn();
//
//		assertEquals(200, result.getResponse().getStatus());
//	}



package exemple.test.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import exemple.test.models.Category;
import exemple.test.models.Product;

public interface CategoryRepository extends MongoRepository<Category, Integer> {
	
	@Query(value = "{'title': {$regex : ?0, $options: 'i'}}")
	public List<Category> findCategoryByTitle(String title);

	public List<Category> findByIdCategoryParent(int catId);
}

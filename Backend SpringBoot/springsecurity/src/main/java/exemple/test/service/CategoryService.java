package exemple.test.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exemple.test.exceptions.ProductNotfoundException;
import exemple.test.exceptions.UserNotfoundException;
import exemple.test.models.*;
import exemple.test.repository.CategoryRepository;
import exemple.test.repository.ProductRepository;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryrepository;

	

	public List<Category> getAllCategories() {
		
		return categoryrepository.findAll();

	}

	public Category getCategoryById(int idCat) {

		Optional<Category> category = categoryrepository.findById(idCat);
		return category.get();

	}
	
	public List<Category> getSousCategories(int idCatParent){
		return categoryrepository.findByIdCategoryParent(idCatParent);
	}
	public void addCategory(Category cat) {
		categoryrepository.save(cat);
	}
	
/*
	public void updateProduct(Product product) {
		Optional< Product > prod = productrepository.findById(product.getId());
		if(!prod.isPresent())
			throw new ProductNotfoundException();
		prod.get().setDescription(product.getDescription());
		prod.get().setTitle(product.getTitle());
		productrepository.save(prod.get());
		
	}

	public void deleteProduct(int idProduct) {
		Optional<Product> prod = productrepository.findById(idProduct);
		if(!prod.isPresent()) {
			throw new ProductNotfoundException();
		}
		productrepository.delete(prod.get());
		
	}
*/
}

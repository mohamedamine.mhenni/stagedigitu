package exemple.test.models;

import java.util.ArrayList;
import java.util.List;

public class Notifications {
	 private int count;
	 private List<String> Message = new ArrayList<>();
	    public Notifications(int count) {
	        this.count = count;
	    }
	    
	    public List<String> getMessage() {
			return this.Message;
		}

		
		public int getCount() {
	        return count;
	    }
	    public void setCount(int count) {
	        this.count = count;
	    }
	    public void increment() {
	        this.count++;
	    }
	   
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';
import {Router} from '@angular/router';
import {User} from '../Response/User';
@Injectable({
  providedIn:'root'
})
export class LoginService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

 public  constructor(private http:HttpClient,private router : Router){
   this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
   this.currentUser = this.currentUserSubject.asObservable();
 }
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }
 public Authenticate(request):Observable<any>{

   return this.http.post<any>("/api/authenticate", JSON.stringify(request),this.httpOptions)
     .pipe(map(user => {
       // store user details and jwt token in local storage to keep user logged in between page refreshes
       sessionStorage.setItem('currentUser', JSON.stringify(user));
       this.currentUserSubject.next(user);
       return user;
     }));
 }

  logout() {
    // remove user from local storage and set current user to null
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

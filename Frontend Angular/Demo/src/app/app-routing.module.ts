import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from "./Helpers/AuthGuarde";
import {CategoryComponent} from "./category/category.component";
import {ProductsComponent} from "./products/products.component";
import {HomeComponent} from "./home/home.component";
import {ProfilComponent} from "./profil/profil.component";
import {UsersComponent} from "./users/users.component";
import {CompteactivationComponent} from "./signup/compteactivation/compteactivation.component";
import {TestMaterialComponent} from "./test-material/test-material.component";
import {SignupComponent} from "./signup/signup.component";
import {LoginComponent} from "./login/login.component";
import {RoleGuarde} from "./Helpers/RoleGuarde";

const routes: Routes = [
  { path: 'login',component: LoginComponent  },
  { path: 'signup',component: SignupComponent  },
  { path: 'test',component: TestMaterialComponent  },
  { path: 'activation',component: CompteactivationComponent  },
  { path: 'profile',component: ProfilComponent ,canActivate : [AuthGuard] },
  { path: 'user',component: UsersComponent , canActivate: [AuthGuard,RoleGuarde] },
  { path: 'home',component: HomeComponent , canActivate: [AuthGuard]  },
  { path: 'product',component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'category',component: CategoryComponent , canActivate: [AuthGuard] },
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

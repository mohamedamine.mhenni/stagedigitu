package exemple.test.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import exemple.test.models.Category;
import exemple.test.models.Product;

public interface ProductRepository extends MongoRepository<Product, Integer>{
  public List<Product> findProductByIdUser(int id);
  @Query(value = "{'title': {$regex : ?0, $options: 'i'}}")
  public List<Product> findProductByTitle(String title); 
  public List<Product> findProductByCategories(List<Category> lc);
	
}

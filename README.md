# Angular Frontend with SpringBoot (Java) Backend
Il s'agit d'une implémentation RESTfull d'une application de traitement des commandes basée sur le schéma de base de données MongoDB. 
Le but du projet est de : 
- Mettre en évidence les techniques de création et de sécurisation d'une application complète REST à l'aide de SpringBoot
- Comment utiliser un service RESTfull et créer une application d à l'aide d'Angular .

### Technologies Utiliser
Component         | Technology
---               | ---
Frontend          | [Angular]
Backend (REST)    | [SpringBoot](Java)
Security          | Token Based (Spring Security and [JWT] )
REST Documentation| [Swagger UI / Springfox]
DataBase          | [MongoDB]
Client Build Tools| [angular-cli], Webpack, npm
Server Build Tools| Maven(Java) 

### Caractéristiques du projet
* Backend
  * Sécurité basée sur les jetons (utilisant Spring security)
  * Documentation de l'API et liens d'essai en direct avec Swagger
  * Utilisation du Mongo Template pour communiquer à la base de données Mongo
  * Comment "request" et "respond" pour manipuler les données 
  * WebSocket Configuration 

* Frontend
  * Organisation des composants, services, directives, pages, etc. dans une application Angular
  * Techniques to Lazy load Data 
  * Routage et protection des pages nécessitant une authentification
  * Basic visulaization
  * Echange des messages avec le serveur à l'aide de socket


# Backend SPRING_BOOT

Une application Spring Boot pour sécuriser une API REST avec un jeton Web JSON (JWT) 

## controle d'accès
### Spring Security

Utilisez le jeton pour accéder aux ressources via votre API RESTful

Accéder au contenu disponible pour tous les utilisateurs authentifiés

Accès selon les roles des utilisateurs
```xml
    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
```
```java
@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http.csrf()
		.disable()
		.authorizeRequests()
		.antMatchers("/api/authenticate").permitAll()
		.antMatchers("/api/signup/**").permitAll()
		.antMatchers("http://ferdaws-dev.digit-dev.com").permitAll()
		.antMatchers("/api/notify").permitAll()
		.antMatchers("/api/users/**").permitAll().hasAuthority("ADMIN")
		.antMatchers("/api/products/**").permitAll().hasAnyAuthority("ADMIN","CLIENT")
		.antMatchers("/api/categories/**")
		.permitAll()
		.antMatchers("/api/users/add").permitAll().antMatchers(
	            "/v2/api-docs", 
	            "/swagger-resources/**",  
	            "/swagger-ui.html", 
	            "/webjars/**" )
		.permitAll()
		.anyRequest()
		.authenticated()
		.and().sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
```
## Documentation de l'API "swagger"
Swagger pour la documentation de l'API REST
```xml
    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
```
### Configuration 

```java
@Configuration
@EnableWebMvc
@EnableSwagger2
public class SwaggerConfig  implements WebMvcConfigurer{
    ...
    ...
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry
                .addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry
                .addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}

```
## Exception personnalisé
```java
@ControllerAdvice
public class UserExceptionController {
	@ExceptionHandler(value = UserNotfoundException.class)
	public ResponseEntity<Object> exception(UserNotfoundException exception) {
		return new ResponseEntity<>("User Not Found", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = UserAlreadyExistException.class)
	public ResponseEntity<Object> exception(UserAlreadyExistException exception) {
		return new ResponseEntity<>("User Already Exist", HttpStatus.BAD_REQUEST);
	}
	
}
```
## Connexion MongoDB
Stocker des objet java dans une base de données MongoDB à l'aide de Spring Data MongoDB.
```xml
    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb</artifactId>
		</dependency>
```
```
#mongodb
spring.data.mongodb.host=localhost
spring.data.mongodb.port=27017
spring.data.mongodb.database=TestDB
```
## WebSocket

Envoie des messages dans les deux sens entre un navigateur et un serveur.

```xml
    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-websocket</artifactId>
		</dependency>
```
### Configuration
```java
@Override
	public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
		stompEndpointRegistry.addEndpoint("/socket")
				.setAllowedOrigins("*")
				.withSockJS();
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker("/topic");
		registry.setApplicationDestinationPrefixes("/app");
	}
```
## Mail

Après l'inscription ,un code d'activation sera généré et envoyé par mail .

```xml
    <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-mail</artifactId>
		</dependency>
```
### Configuration

```java
  @Bean
	    public JavaMailSender getJavaMailSender() {
	        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	        mailSender.setHost("smtp.gmail.com");
	        mailSender.setPort(587);
	 
	        mailSender.setUsername(email);
	        mailSender.setPassword(password);
	 
	        Properties props = mailSender.getJavaMailProperties();
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.debug", "true");
	 
	        return mailSender;
	    }
```

# BackEnd Angular
## Project Structure


```bash
├── app
|  ├── app-routing.module.ts
|  ├── app.component.html
|  ├── app.component.scss
|  ├── app.component.spec.ts
|  ├── app.component.ts
|  ├── app.module.ts
|  ├── helpers
|  |  ├── AuthGuarde.ts
|  |  ├── JwtInterceptor.ts
|  |  └── RoleGuarde.ts
|  |  
|  ├── home
|  ├── login
|  ├── NavBar
|  ├── products
|  ├── profil
|  ├── Response
|  ├── Signup
|  |  └── CompteActivation
|  └── Services
|     ├── CategoryService.ts
|     ├── ProductService.ts
|     ├── CategoryService.ts
|     ├── UserService.ts
|     ├── LoginService.ts
|     ├── WebSocketService.ts
|     └── RegistrationService.ts
├── assets
|  ├── css
|  └── img
├── environments
|  ├── environment.prod.ts
|  └── environment.ts
├── favicon.ico
├── index.html
├── main.ts
├── polyfills.ts
├── styles.scss
└── test.ts
```

## Routage  
```javascript

const routes: Routes = [
  { path: 'login',component: LoginComponent  },
  { path: 'signup',component: SignupComponent  },
  { path: 'test',component: TestMaterialComponent  },
  { path: 'activation',component: CompteactivationComponent  },
  { path: 'profile',component: ProfilComponent ,canActivate : [AuthGuard] },
  { path: 'user',component: UsersComponent , canActivate: [AuthGuard,RoleGuarde] },
  { path: 'home',component: HomeComponent , canActivate: [AuthGuard]  },
  { path: 'product',component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'category',component: CategoryComponent , canActivate: [AuthGuard] },
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

```
## protection des pages qui nécessitant une authentification  

```javascript

canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let user = sessionStorage.getItem('currentUser');
    const currentUser = this.authenticationService.currentUserValue;


    console.log(JSON.stringify(currentUser)==user);
    if (JSON.stringify(currentUser)==user ) {
      // authorised so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigateByUrl('/login');
    sessionStorage.removeItem('currentUser');

    return false;


  }

```
## Techniques to load Data
```javascript
	return this.http.post<any>("/api/authenticate", JSON.stringify(request),this.httpOptions)
		.pipe(map(user => {
		// store user details and jwt token in session storage to keep user logged in between page refreshes
		sessionStorage.setItem('currentUser', JSON.stringify(user));
		this.currentUserSubject.next(user);
		return user;
		}));
```
## Injection et Manipulation des services
```javascript
@Injectable({
  providedIn:'root'
})
export class CategoryService {


  public  constructor(private http:HttpClient ){

  }
  getAllCategories(): Observable<Category[]> {
    // @ts-ignore
    return this.http.get('/api/categories/all')
      .pipe(
        map(
          (jsonArray: any[]) => jsonArray.map(jsonItem => Category.fromJson(jsonItem))
        ),
        retry(1),
        catchError(this.handleError)
      )
  }
  ...
  ...
  ...
}
```
## Manipulation des "Components"
* .ts files
```javascript
	
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	....
	....

}
```
* .html files
```html
 <div class="row" style="margin-top: 10px">
    <div class="col-sm-1">
       <img src="../../assets/img/category.jpg" class="img-thumbnail img" alt="{{c.title}}">
    </div>
    <div class="col-sm-3">
      <p>{{c.title}}</p>
    </div>
    <div class="col-sm-3">
      <p>{{c.description}}</p>
    </div>
    <div class="col-sm-2">
      <p>{{c.parent}}</p>
    </div>
    <div class="col-sm-3">
      <button (click)="deleteCategory(c.id)" class="btn btn-primary mb-2 mr-2">Delete</button>
      <button (click)="openVerticallyCenteredModif(contentmodif);itemmodif(c.id)" class="btn btn-primary mb-2 r-2">Update </button>
    </div>
</div>
```
## JWT-Interceptor 
L'intercepteur JWT se chargera d'envoyer le JWT dans chaque requête.

```javascript
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authenticationService: LoginService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    let currentUser = this.authenticationService.currentUserValue;
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          contentType: 'application/json',
          Authorization: `Bearer ${currentUser.token}`
        }
      });
    }

    return next.handle(request);
  }
}
```
## WebSocketService 
WebSockets in Angular by using SockJS, StompJS
```javascript
initializeWebSocketConnection() {
    const serverUrl = 'http://localhost:8080/socket';
    const ws = new SockJs(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/topic/notification', (message) => {
        if (message.body) {
          //that.notifications= JSON.parse(message.body).count;
          that.notifications=that.notifications+1;
          that.msg=JSON.parse(message.body).message;
        }
      });
    });
  }
```
## ScreenShot
### Swagger

![Swagger](/uploads/a5250a0e23dffa2ed537a1a20e701292/Swagger.png)

### Connexion 

![login](/uploads/e5c92e48287960ed370ff19c4349c737/login.png)

### Inscription

![signup](/uploads/8060404d78fa786801fd45ecbab02463/signup.png)

### Activation de compte

![activationcode](/uploads/c72064fc73c5c0763625d43bc1d765ce/activationcode.png)

### Interface users

![user](/uploads/bd5c9d6ac295782ee855d6665053dcd2/user.png)

### Ajouter un utilisateur

![modal](/uploads/3083073bdae0a935bb45776b94252a65/modal.png)

### Notifications

![notification](/uploads/d074afb0cb107e1d2f79063fc9f0571a/notification.png)

package exemple.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;

import exemple.test.models.Product;
import exemple.test.util.JwtUtil;

@TestInstance(Lifecycle.PER_CLASS)
@SpringBootTest
class ProductApplicationTests {
	@Autowired
	JwtUtil jwtUtil;
	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;
	@Autowired
	private ObjectMapper mapper;

	public Collection<? extends GrantedAuthority> getAuthorities(List<String> roles) {
		return roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

	@BeforeAll
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}
	@Test
	public void testgetProducts() throws Exception {
		List<String> roles = new ArrayList<String>();
		roles.add("ADMIN");
		
		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty",getAuthorities(roles)));
		System.out.println(token);
		assertNotNull(token);
		MvcResult result = mvc.perform(get("/api/products/allproduct/54416").header("Authorization","Bearer "+ token)).andExpect(status().isOk()).andReturn();
		
		assertEquals(200,result.getResponse().getStatus());
	}

	@Test
	public void testgetProductById() throws Exception {
		List<String> roles = new ArrayList<String>();
		roles.add("ADMIN");

		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
		System.out.println(token);
		assertNotNull(token);
		MvcResult result = mvc.perform(get("/api/products/ById/1").header("Authorization", "Bearer " + token))
				.andExpect(status().isOk()).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}
	@Test
	public void testAddProduct() throws Exception {
		List<String> roles = new ArrayList<String>();
		roles.add("ADMIN");
		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
		Product product = new Product(9, "produit de test", "produit de test description");
		product .setIdUser(54456);
		String jsonRequest = mapper.writeValueAsString(product);
		System.out.println(token);
		assertNotNull(token);
		MvcResult result = mvc.perform(post("/api/products/addProduct/2")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest)
				.header("Authorization", "Bearer " + token))
				.andExpect(status().isOk()).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}
	
	@Test
	public void testDeleteProduct() throws Exception {
		List<String> roles = new ArrayList<String>();
		roles.add("ADMIN");
		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
		System.out.println(token);
		assertNotNull(token);
		MvcResult result = mvc.perform(delete("/api/products/deleteProduct/9")
				.header("Authorization", "Bearer " + token))
				.andExpect(status().isOk()).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}
	
	@Test
	public void testUpdateProduct() throws Exception {
		List<String> roles = new ArrayList<String>();
		roles.add("ADMIN");
		String token = jwtUtil.generateToken(new User("medamine@gmail.com", "azerty", getAuthorities(roles)));
		Product product = new Product(1, "produit de test", "produit de test description");
		product .setIdUser(54456);
		String jsonRequest = mapper.writeValueAsString(product);
		assertNotNull(token);
		MvcResult result = mvc.perform(put("/api/products/updateProduct")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonRequest)
				.header("Authorization", "Bearer " + token))
				.andExpect(status().isOk()).andReturn();

		assertEquals(200, result.getResponse().getStatus());
	}

}

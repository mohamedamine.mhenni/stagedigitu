package exemple.test.exceptions;

public class NoDataFoundException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoDataFoundException() {
		super("NoDataFoundException");
	}
}

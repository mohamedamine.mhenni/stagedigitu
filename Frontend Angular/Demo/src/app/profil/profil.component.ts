import { Component, OnInit } from '@angular/core';
import {ProductService} from '../Services/ProductService';
import {FormControl, FormGroup} from "@angular/forms";
import {LoginService} from "../Services/LoginService";
import {User} from "../Response/User";
import {UserService} from "../Services/UserService";

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  userForm: FormGroup;
  hide= true;
  constructor(private AuthService:LoginService,private userService: UserService) {
    this.userForm = new FormGroup({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      username: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl('')

    });
  }
  get firstname() {
    return this.userForm.get('firstname');
  }

  get lastname() {
    return this.userForm.get('lastname');
  }

  get username() {
    return this.userForm.get('username');
  }

  get email() {
    return this.userForm.get('email');
  }

  get password() {
    return this.userForm.get('password');
  }
  public getFormUser(){
    console.log(this.userForm.value);
  }
  ngOnInit() {
   this.loadUserInfo();

  }
  loadUserInfo(){
    this.userService.getUserById(this.AuthService.currentUserValue.id).subscribe(data=>{
      this.userForm.get('firstname').setValue(data['firstname']);
      this.userForm.get('lastname').setValue(data['lastname']);
      this.userForm.get('username').setValue(data['username']);
      this.userForm.get('email').setValue(data['email']);
      this.userForm.get('password').setValue(data['password']);
    })
  }
  updateUser(){

    let user = {
      'id':this.AuthService.currentUserValue.id,
      'firstname': this.firstname.value,
      'lastname': this.lastname.value,
      'username': this.username.value,
      'email': this.email.value,
      'password': this.password.value,
      'roles':[this.AuthService.currentUserValue.role]
    };
    console.log(user);
    window.location.reload();
    this.userService.UpdateUser(user).subscribe(data=>{this.loadUserInfo()})
  }


}

package exemple.test.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import exemple.test.models.*;
import exemple.test.service.MyUserDetailsService;
import exemple.test.service.UserService;
import exemple.test.util.JwtUtil;
import io.jsonwebtoken.MalformedJwtException;

@RestController
@RequestMapping("/api")
public class AuthenticateController {
	@Autowired
	UserService uservice;
	@Autowired
	MyUserDetailsService userservice;
	@Autowired
	private AuthenticationManager authenticationmanager;
	@Autowired
	private JwtUtil jwtTokenUtil;

	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody @Valid AuthenticationRequest authenticationrequest)
			throws Exception {
		try {
			
			authenticationmanager.authenticate(new UsernamePasswordAuthenticationToken(authenticationrequest.getEmail(),
					authenticationrequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("username or password incorrect ", e);
		}
		final UserDetails userDetails = userservice.loadUserByUsername(authenticationrequest.getEmail());
		User u = uservice.getUserByEmail(userDetails.getUsername());
		System.out.println(userDetails);
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		Map<Object, Object> model = new HashMap<>();
		model.put("id", u.getId());
		model.put("email", u.getEmail());
		model.put("token", jwt);
		model.put("role", u.getRoles().get(0));
		model.put("username", u.getUsername());
		return ResponseEntity.ok(model);
	}
	@PostMapping("/validate")
	public Boolean validateToken(@RequestBody Object token){
		System.out.println("--------"+token);
		Map<String , String> tkn = (Map<String, String>) token;
		 System.out.println("-------------------------"+tkn.get("token"));
		try {
			return !jwtTokenUtil.isTokenExpired(tkn.get("token"));
			
		}catch(MalformedJwtException e ) {
			return false;
		}
		 
		
	}
}

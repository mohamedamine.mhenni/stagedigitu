import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn:'root'
})
export class RegistrationService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }


  public  constructor(private http:HttpClient ){

  }


  addUser(User): Observable<any> {
    return this.http.post<any>('api/signup/registration',User,this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  ActivateUser(User): Observable<any> {
    return this.http.post<any>('api/signup/activate',User,this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  reSendMail(id){
    return this.http.get('api/signup/resendMail/'+id,this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

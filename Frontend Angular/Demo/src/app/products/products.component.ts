import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProductService} from '../Services/ProductService';
import {FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CategoryService} from '../Services/CategoryService';
import {LoginService} from "../Services/LoginService";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  encapsulation: ViewEncapsulation.None,
  styles: [`
      .dark-modal .modal-content {
          background-color: #292b2c;
          color: white;
      }

      .dark-modal .modal-contentmodif {
          background-color: #292b2c;
          color: white;
      }

      .dark-modal .close {
          color: white;
      }

      .light-blue-backdrop {
          background-color: #5cb3fd;
      }
  `]
})
export class ProductsComponent implements OnInit {
  lc: any = [];
  products: any = [];
  categories: any = [];
  productForm: FormGroup;
  prodmodif: any;
  Textbox: any;

  constructor(private productservice: ProductService, private Auth: LoginService, private categoryservice: CategoryService, private modalService: NgbModal) {

    this.productForm = new FormGroup({
      title: new FormControl(''),
      description: new FormControl(''),
      category: new FormControl('')
    });
  }

  get getTitle() {
    return this.productForm.get('title');
  }

  get getDescription() {
    return this.productForm.get('description');
  }

  get getCategory() {
    return this.productForm.get('category');
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, {centered: true});
  }

  openVerticallyCenteredModif(contentmodif) {
    this.modalService.open(contentmodif, {centered: true});
  }

  ngOnInit() {
    this.getAllProducts();
    this.getAllCategories();

  }

  getAllProducts() {
    this.productservice.getAllProducts().subscribe((data: {}) => {
      this.products = data;
    });
    console.log(this.products);
  }

  getAllProductsFiltred(title) {
    this.productservice.getAllProductsFiltred(title).subscribe((data: {}) => {
      this.products = data;
    });
    console.log(this.products);
  }

  getAllCategories() {
    this.categoryservice.getAllCategories().subscribe((data: {}) => {
      this.categories = data;
      console.log(data);
    });
    console.log(this.categories);
  }

  getByCate() {
    let cats = this.categories;
    this.productservice.getAllProductsByCategories(cats).subscribe(data => console.log(data))
  }

  addProduct() {
    let product = {
      'title': this.getTitle.value,
      'description': this.getDescription.value,
      'idUser': this.Auth.currentUserValue.id,
    };
    let idCat = this.getCategory.value;
    this.productservice.addProduct(product, idCat).subscribe(data => {
      console.log(data); this.getAllProducts();
    });
  }

  deleteProduct(id) {
    this.productservice.deleteproduct(id).subscribe(data => this.getAllProducts());
  }

  updateProduct() {
    let product = {
      "id": this.prodmodif,
      "title": this.getTitle.value,
      "description": this.getDescription.value
    };
    this.productservice.UpdateProduct(product).subscribe(data => {
      this.getAllProducts()
    })
  }

  itemmodif(id) {
    this.productservice.getProductById(id).subscribe(data => {
      this.productForm.setValue({
        'title': data['title'],
        'description': data['description'],
        'category': ""
      });
      this.prodmodif = id;
      console.log(data);
    });
  }


  clearinputs() {
    this.productForm.get('title').reset();
    this.productForm.get('description').reset();
    this.productForm.get('category').reset();
  }

  textsearchChangedHandler($event) {
    this.Textbox = $event;
    if (this.Textbox)
      this.getAllProductsFiltred(this.Textbox);
    else
      this.getAllProducts()

  }

  catCheck(val, i) {
    console.log(val, i);

    console.log(this.lc.includes(val));
    console.log(this.lc);
    if (!this.lc.includes(val))
      this.lc.push(val);
    else {
      let index = this.lc.indexOf(val);
      console.log(index);
      this.lc.splice(index, 1);
      console.log("-----------------------LC-----------------------");
      console.log(this.lc);
      console.log("------------------------------------------------");
    }

    if (this.lc.length != 0)
      this.productservice.getAllProductsByCategories(this.lc).subscribe(data => {
        console.log(data), this.products = data
      });
    else
      this.getAllProducts()
  }
}

package exemple.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import exemple.test.models.*;
@Service
public class MyUserDetailsService implements UserDetailsService {
	
	@Autowired
	UserService userservice;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User u = userservice.getUserByEmail(email);
		System.out.println(u);
		//return new  org.springframework.security.core.userdetails.User(u.getEmail(), u.getPassword(), u.getAuthorities());
		return new  org.springframework.security.core.userdetails.User(u.getEmail(),u.getPassword(), u.getAuthorities());
	}

}

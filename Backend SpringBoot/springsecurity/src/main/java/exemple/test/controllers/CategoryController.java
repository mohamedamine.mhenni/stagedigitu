package exemple.test.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exemple.test.models.*;
import exemple.test.repository.CategoryRepository;
import exemple.test.service.CategoryService;
import exemple.test.service.UserService;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {
	@Autowired
	CategoryService categorieservice;
	@Autowired
	private CategoryRepository categoryrepository;

	@GetMapping("/all")
	public List<Object> getAllcategories() {
		List<Object> lc = new ArrayList<>();
		for (Category cat : categorieservice.getAllCategories()) {
			if (cat.getIdCategoryParent() != 0) {
				Category c = categorieservice.getCategoryById(cat.getIdCategoryParent());
				Map<Object, Object> model = new HashMap<>();
				model.put("id", cat.getId());
				model.put("title", cat.getTitle());
				model.put("description", cat.getDescription());
				model.put("parent", c.getTitle());
				model.put("idParent", cat.getIdCategoryParent());
				lc.add(model);
			}else if(cat.getIdCategoryParent() == 0)
			{
				Map<Object, Object> model = new HashMap<>();
				model.put("id", cat.getId());
				model.put("title", cat.getTitle());
				model.put("description", cat.getDescription());
				model.put("parent", "No Parent");
				model.put("idParent", cat.getIdCategoryParent());
				lc.add(model);
			}
		}

		return lc;
	}

	@GetMapping("ById/{idCat}")
	public Category getCatById(@PathVariable int idCat) {
		return categoryrepository.findById(idCat).get();
	}
	public int getNewId() {
		List<Category> lc =this.categoryrepository.findAll().stream().sorted(Comparator.comparing(Category::getId)).collect(Collectors.toList());
		System.out.println(lc);
		return lc.get(lc.size()-1).getId()+1;
	}
	@GetMapping("ByTitle/{title}")
	public List<Object> getCategoryByTitle(@PathVariable String  title) {
		List<Object> lc = new ArrayList<>();
		for (Category cat : categoryrepository.findCategoryByTitle(title)) {
			if (cat.getIdCategoryParent() != 0) {
				Category c = categorieservice.getCategoryById(cat.getIdCategoryParent());
				Map<Object, Object> model = new HashMap<>();
				model.put("id", cat.getId());
				model.put("title", cat.getTitle());
				model.put("description", cat.getDescription());
				model.put("parent", c.getTitle());
				model.put("idParent", cat.getIdCategoryParent());
				lc.add(model);
			}else if(cat.getIdCategoryParent() == 0)
			{
				Map<Object, Object> model = new HashMap<>();
				model.put("id", cat.getId());
				model.put("title", cat.getTitle());
				model.put("description", cat.getDescription());
				model.put("parent", "No Parent");
				model.put("idParent", cat.getIdCategoryParent());
				lc.add(model);
			}
		}
		return lc;
	}
	@PostMapping("/add")
	public ResponseEntity<Object> addCategory(@RequestBody Category category) {
		category.setId(this.getNewId());
		categorieservice.addCategory(category);
		return new ResponseEntity<>(category, HttpStatus.OK);
	}

	@GetMapping("/sousCategorie/{ParentCat}")
	public List<Category> getAllSousCategoriesById(@PathVariable int ParentCat) {
		return categorieservice.getSousCategories(ParentCat);
	}

	@DeleteMapping("/Delete/{idCat}")
	public ResponseEntity<Object> DeleteCategory(@PathVariable int idCat) {
		categoryrepository.deleteById(idCat);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	@PutMapping("/Update")
	public ResponseEntity<Object> UpdateCategory(@RequestBody Category cat) {
		categoryrepository.save(cat);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}

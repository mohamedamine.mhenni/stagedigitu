package exemple.test.controllers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exemple.test.models.*;
import exemple.test.repository.UserRepository;
import exemple.test.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	@Autowired
	UserService userservice;
	@Autowired
	UserRepository userrepository;
	@GetMapping("/all")
	public List<User> getAllUsers() {
		return userservice.getAllUsers();
	}
	public int getNewId() {
		List<User> lc =this.userrepository.findAll().stream().sorted(Comparator.comparing(User::getId)).collect(Collectors.toList());
		System.out.println(lc);
		return lc.get(lc.size()-1).getId()+1;
	}
	@GetMapping("/ByUserName/{username}")
	public List<User> getAllUsersByUserName(@PathVariable String username) {
		return userrepository.findByUserName(username);
	}

	@GetMapping("ByEmail/{email}")
	public ResponseEntity<Object> getUserByEmail(@PathVariable String email) {

		return new ResponseEntity<>(userservice.getUserByEmail(email), HttpStatus.OK);
	}

	
	@GetMapping("ById/{idUser}")
	public ResponseEntity<Object> getUserById(@PathVariable int idUser) {
		return new ResponseEntity<>(userservice.getUserById(idUser), HttpStatus.OK);

	}

	@PostMapping("/add")
	public ResponseEntity<Object> addUser(@RequestBody User user) {
		user.setId(getNewId());
		userservice.addUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@DeleteMapping(value="/{idUser}",produces = MediaType.ALL_VALUE)
	public ResponseEntity<Object> deleteUserById(@PathVariable int idUser) {
		userservice.deleteUser(idUser);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value="/updateUser")
	public ResponseEntity<Object> updateUser(@RequestBody User user) {
		userservice.updateUser(user);
	
		return new ResponseEntity<Object>( HttpStatus.OK);
	}

}

package exemple.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import exemple.test.filters.JwtRequestFilter;
import exemple.test.service.MyUserDetailsService;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	MyUserDetailsService myUserDetailsService;
	@Autowired
	JwtRequestFilter jwtRequestFilter;
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub

		super.configure(auth);
		auth.userDetailsService(myUserDetailsService);
	}
	
	
	
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		// TODO Auto-generated method stub
		return super.authenticationManagerBean();
	}

	

	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http.csrf()
		.disable()
		.authorizeRequests()
		.antMatchers("/api/authenticate")
		.permitAll()
		.antMatchers("/api/signup/**")
		.permitAll()
		.antMatchers("http://ferdaws-dev.digit-dev.com")
			.permitAll()
		//.requestMatchers(req-> req.getRequestURI().contains("socket")).permitAll()	
		.antMatchers("/socket/**").permitAll()
		.antMatchers("/api/notify").permitAll()
		.antMatchers("/api/users/**").permitAll()
		//.hasAuthority("ADMIN")
		.antMatchers("/api/products/**").permitAll()
		//.hasAnyAuthority("ADMIN","CLIENT")
		.antMatchers("/api/categories/**")
		.permitAll()
		.antMatchers("/api/users/add")
		.permitAll()
		.antMatchers(
	            "/v2/api-docs", 
	            "/swagger-resources/**",  
	            "/swagger-ui.html", 
	            "/webjars/**" )
		.permitAll()
		.anyRequest()
		.authenticated()
		.and().sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
}

package exemple.test.models;

public class ActivateResponse {
	private String code;
	private int id;
	public ActivateResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ActivateResponse(String code, int id) {
		super();
		this.code = code;
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "ActivateResponse [code=" + code + ", id=" + id + "]";
	}
	
	
}

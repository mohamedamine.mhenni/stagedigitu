import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CategoryService} from '../Services/CategoryService';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';
import {Category} from '../Response/Category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  encapsulation: ViewEncapsulation.None,
  styles: [`
      .dark-modal .modal-content {
          background-color: #292b2c;
          color: white;
      }

      .dark-modal .modal-contentmodif {
          background-color: #292b2c;
          color: white;
      }

      .dark-modal .close {
          color: white;
      }

      .light-blue-backdrop {
          background-color: #5cb3fd;
      }
  `]
})
export class CategoryComponent implements OnInit {
  tt: any[] = [];
  parent;
  categories: Category[];
  Textbox: any;
  CategoryForm: FormGroup;
  itemmod: any;
  souscat: any;

  constructor(private categoryservice: CategoryService, private modalService: NgbModal) {
    this.CategoryForm = new FormGroup({
      title: new FormControl(''),
      description: new FormControl(''),
      parent: new FormControl('')
    });


  }

  get getTitle() {
    return this.CategoryForm.get('title');
  }

  get getDescription() {
    return this.CategoryForm.get('description');
  }

  get getParent() {
    return this.CategoryForm.get('parent');
  }

  getCategoryForm() {
    return this.CategoryForm.value;
  }

  ngOnInit() {
    this.getAllcategories();
  }


  getAllcategories() {
    this.categoryservice.getAllCategories().subscribe((data: Category[]) => {
      this.categories = data;
      console.log(data);
    });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, {centered: true});
  }

  deleteCategory(id) {
    this.categoryservice.deleteCategory(id).subscribe(data => {
      console.log(data);
      this.getAllcategories();
    });
  }

  getSousCategorie(id) {
    this.categoryservice.getSousCategorie(id).subscribe(data => this.souscat = data);
  }

  openVerticallyCenteredModif(contentmodif) {
    this.modalService.open(contentmodif, {centered: true});
  }

  openVerticallyCenteredMychild(contentMyChild) {
    this.modalService.open(contentMyChild, {centered: true});
  }

  itemmodif(id: any) {
    this.categoryservice.getCategoryById(id).subscribe(data => {
      this.CategoryForm.setValue({
        'title': data['title'],
        'description': data['description'],
        'parent': data['idCategoryParent']
      });
    });
    this.itemmod = id;
  }

  textsearchChangedHandler($event) {
    this.Textbox = $event;
    if(this.Textbox)
      this.getAllCategoriesFiltred(this.Textbox);
    else
      this.getAllcategories();
  }
  getAllCategoriesFiltred(title) {
    this.categoryservice.getAllCategoriesFiltred(title).subscribe((data:Category[]) => {
      this.categories = data;
    });
    console.log(this.categories);
  }
  addCategory() {
    let category = {
      'id': 25,
      'title': this.getTitle.value,
      'description': this.getDescription.value,
      'idCategoryParent': this.getParent.value
    };
    this.categoryservice.addCategory(category).subscribe(data => {
      console.log(data);
      this.getAllcategories();
    });
  }

  clearinputs() {
    this.CategoryForm.get('title').reset();
    this.CategoryForm.get('description').reset();
    this.CategoryForm.get('parent').reset();
  }


  updateProduct() {
    let category = {
      'id': this.itemmod,
      'title': this.getTitle.value,
      'description': this.getDescription.value,
      'idCategoryParent': this.getParent.value
    };
    this.categoryservice.UpdateCategory(category).subscribe(data => {
      console.log(data);
      this.getAllcategories();
    });
  }
}

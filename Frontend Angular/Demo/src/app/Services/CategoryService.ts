import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';
import {Category} from '../Response/Category';
@Injectable({
  providedIn:'root'
})
export class CategoryService {


  public  constructor(private http:HttpClient ){

  }
  getAllCategories(): Observable<Category[]> {
    // @ts-ignore
    return this.http.get('/api/categories/all')
      .pipe(
        map(
          (jsonArray: any[]) => jsonArray.map(jsonItem => Category.fromJson(jsonItem))
        ),
        retry(1),
        catchError(this.handleError)
      )
  }
  getAllCategoriesFiltred(title): Observable<any> {
    return this.http.get<any>('api/categories/ByTitle/'+title)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
 getSousCategorie(id){
   return this.http.get('api/categories/sousCategorie/'+id).pipe(
     retry(0),
     catchError(this.handleError)
   )
 }
  addCategory(Category): Observable<any> {
    return this.http.post<any>('api/categories/add',Category)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  deleteCategory(id){
    return this.http.delete<any>('api/categories//Delete/'+id)
  }
  getCategoryById(id): Observable<any> {
    return this.http.get<any>('api/categories/ById/'+id)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  UpdateCategory(category): Observable<any> {
    console.log(category);
    return this.http.put<any>('/api/categories/Update', category)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

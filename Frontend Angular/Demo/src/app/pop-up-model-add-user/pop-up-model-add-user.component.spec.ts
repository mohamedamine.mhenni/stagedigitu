import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopUpModelAddUserComponent } from './pop-up-model-add-user.component';

describe('PopUpModelAddUserComponent', () => {
  let component: PopUpModelAddUserComponent;
  let fixture: ComponentFixture<PopUpModelAddUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopUpModelAddUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopUpModelAddUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

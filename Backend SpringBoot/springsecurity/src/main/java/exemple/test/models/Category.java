package exemple.test.models;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;



@Document
public class Category {
	@Id
	private int id;
	
	private String title;
	
	private String Description;
		
	private int idCategoryParent;
	
	public Category() {
	
	}
	public Category(int id, String title, String description) {
		super();
		this.id = id;
		this.title = title;
		Description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	public int getIdCategoryParent() {
		return idCategoryParent;
	}
	public void setIdCategoryParent(int idCategoryParent) {
		this.idCategoryParent = idCategoryParent;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", title=" + title + ", Description=" + Description + ", idCategoryParent="
				+ idCategoryParent + "]";
	}
	
	
	

	
	
	
}

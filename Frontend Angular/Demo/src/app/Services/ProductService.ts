import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

import {LoginService} from './LoginService';

@Injectable({
  providedIn: 'root'
})
export class ProductService {


  public constructor(private http: HttpClient, private authservice: LoginService) {

  }

  getAllProductsByUser(): Observable<any> {
    return this.http.get<any>('api/products/allproduct/' + this.authservice.currentUserValue.id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getAllProducts(): Observable<any> {
    return this.http.get<any>('api/products/allproduct/' + this.authservice.currentUserValue.id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getAllProductsByCategories(categories): Observable<any> {
    console.log(categories);
    return this.http.post<any>('api/products/ByCategories/' + this.authservice.currentUserValue.id, categories)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  getAllProductsFiltred(title): Observable<any> {
    return this.http.get<any>('api/products/ByTitle/' + title)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  addProduct(product, idcat): Observable<any> {
    let prod = {
      "title": product.title,
      "description": product.description,
      "idUser": this.authservice.currentUserValue.id,
    };
    return this.http.post<any>('api/products/addProduct/' + idcat, product)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  deleteproduct(id) {
    return this.http.delete<any>('api/products/deleteProduct/' + id)
  }

  getProductById(id): Observable<any> {
    return this.http.get<any>('api/products/ById/' + id)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  UpdateProduct(product): Observable<any> {
    console.log(product);
    return this.http.put<any>('api/products/updateProduct', product)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

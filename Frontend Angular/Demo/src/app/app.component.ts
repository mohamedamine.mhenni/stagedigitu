import { Component } from '@angular/core';
import {WebSocketService} from "./Services/WebSocketService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Demo';

  public  notifications =0;
  stompClient: any;
  constructor(private webSocketService: WebSocketService) {



  }
  getNotification(){


    this.stompClient = this.webSocketService.connect();
    this.stompClient.connect({}, frame => {

      // Subscribe to notification topic
      this.stompClient.subscribe('/topic/notification', notifications => {
        console.log(notifications);
        // Update notifications attribute with the recent messsage sent from the server
        this.notifications = JSON.parse(notifications.body).count;
      });
    });
  }
}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {LoginService} from '../Services/LoginService';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {throwError} from 'rxjs';
import {state} from '@angular/animations';
import {User} from '../Response/User';


@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  private CurrentUser;

  constructor(
    private http: HttpClient,
    private router:Router,
    private authenticationService: LoginService
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let user = sessionStorage.getItem('currentUser');
    const currentUser = this.authenticationService.currentUserValue;


    console.log(JSON.stringify(currentUser)==user);
    if (JSON.stringify(currentUser)==user ) {
      // authorised so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigateByUrl('/login');
    sessionStorage.removeItem('currentUser');

    return false;


  }


}

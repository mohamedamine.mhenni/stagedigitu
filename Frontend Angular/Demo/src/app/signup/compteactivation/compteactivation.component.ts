import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {RegistrationService} from '../../Services/RegistrationService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-compteactivation',
  templateUrl: './compteactivation.component.html',
  styleUrls: ['./compteactivation.component.css']
})
export class CompteactivationComponent implements OnInit {
  activationForm: FormGroup;

  constructor(private registrationservice : RegistrationService,private router :Router) {
    this.activationForm = new FormGroup({
      code: new FormControl('')
    });
  }

  get code() {
    return this.activationForm.get('code');
  }

  ngOnInit() {
  }

  Activate() {
    let user ={
      "id":sessionStorage.getItem('registredId'),
      "code":this.code.value
    }
    this.registrationservice.ActivateUser(user).subscribe(data=>{
      console.log(data);
      if(data==false)
        alert("Code invalide")
      else
      this.router.navigateByUrl('/login');


    })
  }
  resendMail(){
    this.registrationservice.reSendMail(sessionStorage.getItem('registredId')).subscribe(data=>{
      console.log(data);
    })
  }
}

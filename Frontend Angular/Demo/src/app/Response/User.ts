export class User {
  public static fromJson(json: Object): User {
    return new User(

      json['id'],
      json['email'],
      json['role'],
      json['token'],
      json['username']

    );
  }

  constructor(public id: bigint,
              public email: string,
              public role: string,
              public token: string,
              public username: string
              ) {
  }
}

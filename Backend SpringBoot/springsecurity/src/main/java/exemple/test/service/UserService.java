package exemple.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import exemple.test.repository.ProductRepository;
import exemple.test.repository.UserRepository;
import exemple.test.exceptions.UserAlreadyExistException;
import exemple.test.exceptions.UserNotfoundException;
import exemple.test.models.*;

@Service
public class UserService {
	@Autowired
	UserRepository userrepository;
	@Autowired
	ProductRepository productrepository;

	public List<User> getAllUsers() {
		return userrepository.findAll();
	}

	public User getUserById(int id) {

		Optional<User> user = userrepository.findById(id);

		if (!user.isPresent())
			throw new UserNotfoundException();

		return user.get();

	}

	public User getUserByEmail(String email) {
		Optional< User> user = userrepository.findUserByEmail(email);
		if(!user.isPresent()) 
			throw new UserNotfoundException();
		return user.get();
	}
	
	public User getUserByUserName(String username) {
		Optional< User> user = userrepository.findByUsername(username);
		if(!user.isPresent()) 
			throw new UserNotfoundException();
		return user.get();
		
	}

	public void addUser(User user) {
		Optional<User> u = userrepository.findById(user.getId());
		if(u.isPresent())
			throw new UserAlreadyExistException();
			userrepository.save(user);
		

	}

	public void updateUser(User user) {
		Optional< User> u = userrepository.findById(user.getId());
		if(!u.isPresent()) 
			throw new UserNotfoundException();
		
		userrepository.save(user);
	}

	public void deleteUser(int id) {
		Optional< User> u = userrepository.findById(id);
		if(!u.isPresent()) 
			throw new UserNotfoundException();
		userrepository.deleteById(id);
	}

	/*public Boolean authentification(String email, String password) {
		User u = userrepository.findUserByEmail(email);
		if (u.getPassword().equals(password))
			return true;
		return false;

	}
	/*
	 * public UserDetails loadUserByEmail(String email) throws
	 * UsernameNotFoundException { // TODO Auto-generated method stub User u =
	 * userrepository.findUserByEmail(email); return new
	 * org.springframework.security.core.userdetails.User(u.getEmail(),
	 * u.getPassword(), u.getAuthorities()); }
	 */
}

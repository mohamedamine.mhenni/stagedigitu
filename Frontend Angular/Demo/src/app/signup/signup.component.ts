import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../Services/UserService';
import {RegistrationService} from '../Services/RegistrationService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  hide=true;
  overlay: string;
  loading: any=false;

  constructor(private signupservice: RegistrationService,private router : Router) {
    this.signupForm = new FormGroup({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      username: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl('')

    });
  }
  get firstname() {
    return this.signupForm.get('firstname');
  }

  get lastname() {
    return this.signupForm.get('lastname');
  }

  get username() {
    return this.signupForm.get('username');
  }

  get email() {
    return this.signupForm.get('email');
  }

  get password() {
    return this.signupForm.get('password');
  }
  ngOnInit() {
  }
  addUser() {
    this.loading=true;
    let user = {
      'firstname': this.firstname.value,
      'lastname': this.lastname.value,
      'username': this.username.value,
      'email': this.email.value,
      'password': this.password.value,
      'roles': ['Client']
    };
    this.signupservice.addUser(user).subscribe(data => {
       console.log(data);sessionStorage.setItem('registredId',data['id']);this.router.navigateByUrl('/activation').then(r=>r);

    });
    console.log(user);

  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {ProductsComponent} from "./products/products.component";
import {CompteactivationComponent} from "./signup/compteactivation/compteactivation.component";
import {SignupComponent} from "./signup/signup.component";
import {ProfilComponent} from "./profil/profil.component";
import {TestMaterialComponent} from "./test-material/test-material.component";
import {HomeComponent} from "./home/home.component";
import {CategoryComponent} from "./category/category.component";
import {NavbarComponent} from "./navbar/navbar.component";
import {PopUpModelAddUserComponent} from "./pop-up-model-add-user/pop-up-model-add-user.component";
import {UsersComponent} from "./users/users.component";
import {LoginComponent} from "./login/login.component";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatSliderModule} from "@angular/material/slider";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatBadgeModule} from "@angular/material/badge";
import {WebSocketService} from "./Services/WebSocketService";
import {MatMenuModule} from "@angular/material/menu";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    PopUpModelAddUserComponent,
    NavbarComponent,
    ProductsComponent,
    CategoryComponent,
    HomeComponent,
    TestMaterialComponent,
    ProfilComponent,
    SignupComponent,
    CompteactivationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatMenuModule
  ],
  providers: [WebSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }

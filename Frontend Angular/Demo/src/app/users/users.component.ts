import {Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation} from '@angular/core';
import {UserService} from '../Services/UserService';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';
import {User} from '../Response/User';
import {LoginService} from '../Services/LoginService';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  encapsulation: ViewEncapsulation.None,
  styles: [`
      .dark-modal .modal-content {
          background-color: #292b2c;
          color: white;
      }
      .dark-modal .modal-contentmodif {
          background-color: #292b2c;
          color: white;
      }

      .dark-modal .close {
          color: white;
      }

      .light-blue-backdrop {
          background-color: #5cb3fd;
      }
  `]
})
export class UsersComponent implements OnInit {
  currentUser: User;
  users: any = [];
  closeResult: string;
  userForm: FormGroup;
  userModifId:bigint;
  Textbox: any;
  returnUrl: string;
  displayedColumns: string[] = ['firstname', 'lastname', 'username', 'email'];
  dataSource = new MatTableDataSource<any>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private AuthService: LoginService,private Userservice: UserService,private route: Router, private modalService: NgbModal) {
    this.currentUser = this.AuthService.currentUserValue;
    this.userForm = new FormGroup({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      username: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl('')

    });
  }

  get firstname() {
    return this.userForm.get('firstname');
  }

  get lastname() {
    return this.userForm.get('lastname');
  }

  get username() {
    return this.userForm.get('username');
  }

  get email() {
    return this.userForm.get('email');
  }

  get password() {
    return this.userForm.get('password');
  }

  ngOnInit() {
    this.getAllUsers();

  }

  getAllUsers() {
    this.Userservice.getAllUsers().subscribe((data: [{}]) => {
      this.users = data;this.dataSource=new MatTableDataSource<any>(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    console.log(this.users);
  }
  getAllUsersByUserName(text) {
    this.Userservice.getAllUsersByUserName(text).subscribe((data: {}) => {
      this.users = data;
    });
    console.log(this.users);
  }

  deleteUser(id) {
    this.Userservice.deleteUser(id).subscribe((data: {}) => {
      this.getAllUsers(), console.log(data);
    });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, {centered: true});
  }
  openVerticallyCenteredModif(contentmodif) {
    this.modalService.open(contentmodif, {centered: true});
  }

  addUser() {
    let user = {
      'firstname': this.firstname.value,
      'lastname': this.lastname.value,
      'username': this.username.value,
      'email': this.email.value,
      'password': this.password.value,
      'roles': ['Client']
    };
    this.Userservice.addUser(user).subscribe(data => {
      this.getAllUsers(), console.log(data), this.clearinputs();
    });
    console.log(user);

  }
updateUser(){

  let user = {
    'id':this.userModifId,
    'firstname': this.firstname.value,
    'lastname': this.lastname.value,
    'username': this.username.value,
    'email': this.email.value,
    'password': this.password.value,
    'roles': ['Client']
  };
  console.log(user);
  this.Userservice.UpdateUser(user).subscribe(data=>{this.getAllUsers(), this.clearinputs();})
}
  clearinputs() {

      this.userForm.get('firstname').reset();
      this.userForm.get('lastname').reset();
      this.userForm.get('username').reset();
      this.userForm.get('password').reset();
      this.userForm.get('email').reset();

  }

  itemmodif(id) {
    this.Userservice.getUserById(id).subscribe(data => {
      this.userForm.setValue({
        'email': data['email'],
        'firstname': data['firstname'],
        'lastname': data['lastname'],
        'username': data['username'],
        'password': data['password']
      });
      this.userModifId=data['id'];
      console.log(data)
    });


  }

  textsearchChangedHandler($event) {
    this.Textbox = $event;
    if(this.Textbox)
      this.getAllUsersByUserName(this.Textbox)
    else
      this.getAllUsers();
  }
}
export interface PeriodicElement {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
}

package exemple.test.validation;

import com.google.common.base.Joiner;
import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Digits;

import java.util.Arrays;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public void initialize(final ValidPassword arg0) {

    }

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
    	System.out.println(password+"--------****************---------------");
    	PasswordValidator passwordValidator = new PasswordValidator(Arrays.asList(new LengthRule(6),new RepeatCharacterRegexRule(),new CharacterRule(EnglishCharacterData.Digit)));
        final RuleResult result = passwordValidator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(Joiner.on("\n").join(passwordValidator.getMessages(result))).addConstraintViolation();
        return false;
    }

}
